Pipeline of PATE
1) Pate orchestrator runs the train_teachers script to train all the teachers. 
   This script generates a model for every teacher
   Inside train_teachers script, all the teachers are trained, in a for loop:
   a)For each teacher, a certain partition of the raw data is loaded and converted to floats numbers using a tokenizer
   b)Then, the script builds the model, trains it and saves it for future tasks
   c)This process is repeated until all the teachers are trained
   
2) Then, the orchestrator runs the train student script
   a)First task is to get the dataset of the student
   b)Afterwards, the dataset is converted to float numbers using a tokenizer
   c)Then, every teacher model computes predictions on top of the student's dataset
   d)These predictions are aggregated and treated as votes. 
     On top of these votes, a laplacian noise is added to insert privacy.
   e)Then, the script builds the student model, trains it on the student data and saves it for future tasks. 
	 This is the final trained model.