from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import numpy as np
from tensorflow.keras.optimizers import SGD
import partition
import train_NN_for_PATE


def train_mp_teachers(nb_teachers, train_dir, data_events):
    """ this is a for loop because it runs locally.
    This should run without loop in case of many clients
    :param nb_teachers: number of teachers (in the ensemble) to learn from
    :param teacher_id: Id of the teacher been trained
    :param train_dir: Training directory
    :param data_events: Training data
    """
    for t in range(0, nb_teachers):
        teacher_id = t
        print('teacher_id', t, 'ID of teacher being trained.')
        train_teacher(nb_teachers, teacher_id, train_dir, data_events)


def train_teacher(nb_teachers, teacher_id, train_dir, data_events):
    """
    This function trains a single teacher model with responds teacher's ID among an ensemble of nb_teachers
    models for the dataset specified.
    The model will be save in directory.
    :param nb_teachers: total number of teachers in the ensemble
    :param teacher_id: id of the teacher being trained
    :param train_dir: Training directory
    :param data_events: Training data
    :return: True if everything went well
    """
    # Load the dataset
    raw_data, num_classes = train_NN_for_PATE.get_dataset(data_events)

    # Retrieve subset of data for this teacher
    data = partition.partition_dataset(raw_data, nb_teachers, teacher_id)

    print("Length of training data: " + str(len(data.Priority)))

    # Define teacher checkpoint filename and full path

    model_checkpoint_name_hdf5 = train_dir + '/' + str(nb_teachers) + '_teachers_' + str(teacher_id) + '.hdf5'
    model_checkpoint_name_h5 = train_dir + '/' + str(nb_teachers) + '_teachers_' + str(teacher_id) + '.h5'

    # Perform teacher training need to modify
    data_for_train, data_for_test, labels_for_train, labels_for_test = train_NN_for_PATE.prepare_data(data)
    # variable explanation
    data_for_train = np.asarray(data_for_train)
    labels_for_train = np.asarray(labels_for_train)

    """
    this function gets model configuration
    param input_size: Size of the input data
    param hidden_size: Size of the hidden layer
    param num_classes: Number of Classes
    param dropout: Dropout value for the NN
    param num_epochs: Number of training epochs
    param batch_size: Size of the batch for the NN
    param learning_rate: Learning rate value
    """

    input_size, hidden_size, dropout, num_epochs, batch_size, learning_rate = \
        train_NN_for_PATE.get_model_config(data_for_train)

    # Create teacher model
    model = train_NN_for_PATE.build_nn(input_size, hidden_size, num_classes)
    criterion = 'categorical_crossentropy'
    # opt = Adam(lr=learning_rate)
    opt = SGD(lr=learning_rate, momentum=0.9, clipvalue=0.5)
    model.compile(loss=criterion,
                  optimizer=opt,
                  metrics=['accuracy'])
    # Train the model
    model, hist = train_NN_for_PATE.train(model, batch_size, num_epochs, data_for_train, labels_for_train,
                                          model_checkpoint_name_hdf5)
    # Save the model weights into a json
    model_json = model.to_json()
    with open("model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights(model_checkpoint_name_h5)
    print("Saved model to disk")
    os.remove("model.json")
    """
         Perform teacher training need to modify predictions comment
    encoder = LabelBinarizer()
    encoder.fit(Labels_for_test)
    predictions = train_NN_for_PATE.softmax_preds(model, Data_for_test, Labels_for_test, encoder)
    print('Acc:' + str(predictions))
    """
    return True


if __name__ == '__main__':
    train_mp_teachers()
